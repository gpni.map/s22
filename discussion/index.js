//console.log("S22 Discussion");


// JAVASCRIPT - ARRAY MANIPULATIONS

// ARRAY METHODS

/*
	Mutator Methods:
		- seeks to modify the contents of an array.
		- are functions that mutate an array after they are created. These methods manipulate original array performing various task such as adding or removing elements.

*/

let fruits = ['Apple', 'Orange', 'Kiwi', 'Watermelon'];

/*
	push() - adds an element at the end of an array and returns the array's length

	syntax:
		arrayName.push(element);

*/

console.log("Current Fruits Array: ");
console.log(fruits);


// Adding items

fruits.push('Mango');
console.log(fruits);

let fruitsLength = fruits.push("Melon");
console.log(fruitsLength);
console.log(fruits);


fruits.push("Avocado", "Guava");
console.log(fruits);



//deleting items in an array

/*
	pop() = removed the last element in our array and returns the removed element.(when applied inside a variable)

	syntax:

		arrayName.pop();

*/


let removeFruit = fruits.pop();
console.log(removeFruit);
console.log("Mutated array from the pop methoed");
console.log(fruits);



/*
	unshift() 	- adds one or more elements at the beginning of an array
				- returns the length of the array (when presented inside a variable.)


	Syntax:

		arrayName.unshift(elementA);
		arrayName.unshift(elementA, Element B);

*/


fruits.unshift("Lime", "Banana");
console.log("Mutated array from the unshift method:")
console.log(fruits);



/*
	shift() - removes an element at the beginning of our array and return the removed element.

	syntax:
		arrayNames.shift();

*/


let removedFruit2 = fruits.shift();
console.log(removedFruit2);
console.log("Mutated array from the shift method");
console.log(fruits);


/*
	splice() - allows to simultaneously remove elements from a specified index number and adds an element.

	Syntax:

		arrayName.splice(startingIndex, deleteCourt, elementsToBeAdded);
*/


let fruitsSplice = fruits.splice(1, 2, "Cherry", "Lychee");
console.log("Mutated arrays from Splice Method:")
console.log(fruits);


// Using splice but without adding elemets
	let removedSplice = fruits.splice(3, 2);
	console.log(fruits);
	console.log(removedSplice);


/*
	sort() - it rearranges the array elements in alphanumeric order

	Syntax:

		arrayName.sort()


*/


// fruits.sort();

console.log("Mutated array from sort method");
console.log(fruits);



let mixedArr = [12, "May", 36, 94, "August", 5, 6.3, "September"];
console.log(mixedArr.sort());


/*
	reverse() - reverses the order of the elements in an array

	Syntax:

		arrName.reverse();

*/


fruits.reverse();
console.log("Mutated array from reverse method");
console.log(fruits);



//for sorting the items in descending order:


fruits.sort().reverse();
console.log(fruits);




/*MINI ACTIVITY 1:
	 - Debug the function which will allow us to list fruits in the fruits array.
	 	-- this function should be able to receive a string.
	 	-- determine if the input fruit name already exist in the fruits array.
	 		*** If it does, show an alert message: "Fruit already listed on our inventory".
	 		*** If not, add the new fruit into the fruits array ans show an alert message: "Fruit is not listed in our inventory."
	 	-- invoke and register a new fruit in the fruit array.
	 	-- log the updated fruits array in the console


	 	function registerFruit () {
	 		let doesFruitExist = fruits.includes(fruitName);

	 		if(doesFruitExst) {
	 			alerat(fruitName "is already on our inventory")
	 		} else {
	 			fruits.push(fruitName);
	 			break;
	 			alert("Fruit is now listed in our inventory")
	 		}
	 	}
*/

	 	// function registerFruit (fruitName) {
	 	// 	let doesFruitExist = fruits.includes(fruitName);

	 	// 	if(doesFruitExist) {
	 	// 		alert(fruitName + " is already on our inventory");
	 	// 	} else {
	 	// 		fruits.push(fruitName);
	 	// 		// break;
	 	// 		alert("Fruit is now listed in our inventory");
	 	// 	}
	 	// }


	 	// registerFruit("Dragon Fruit");
	 	// console.log(fruits);



//MINI ACTIVITY 2:

/* MINI ACTIVITY - Array Mutators
	- Create an empty array
	- use the push() method and unshift() method to add elements in your array
	- use the pop() and shift() method to remove elements in your array
	- use sort to organize your array in alphanumeric order
	- at the end the array should contain not less than 6 elements after applying these methods.
	- log the changes in the console every after update

*/

console.log("MINI ACTIVITY 2")


let cars = ["Ferrari", "Lamborghini", "Maserati", "Toyota", "Nissan"];

console.log("Original Array:")
console.log(cars);

cars.push("Audi", "Honda");
console.log("Updated cars array: ")
console.log(cars);

cars.unshift("Tesla");
console.log("Updated cars array (after unshift): ")
console.log(cars);

cars.pop();
console.log("Updated cars array (after pop): ")
console.log(cars);

cars.shift();
console.log("Updated cars array (after shift): ")
console.log(cars);

cars.sort();
console.log("Updated cars array (after sort): ")
console.log(cars);


/*
	Non-Mutator Methods
		- These are functtions or methods that do not modify or change an array after they are created.
		- These methods do not manipulate the original array performing various tasks such as returning elements from an array and combining arrays and printing the output.
*/

let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];

console.log(countries);

/*
	indexOf() - it returns the index number of the first matching element found in an array. If no match is found, the result will be -1. The search process will be done from our first element proceeding to the last element.

	Syntax:

	arrayName.indexOf(searchValue);
	arrayName.indexOf(searchValue, fromIndex)
*/


let firstIndex = countries.indexOf("PH");
console.log("Result of indexOf method: " + firstIndex);

firstIndex = countries.indexOf("PH", 4);
console.log("Result of indexOf method: " + firstIndex);

firstIndex = countries.indexOf("PH", 7);
console.log("Result of indexOf method: " + firstIndex);

firstIndex = countries.indexOf("PH", -1);
console.log("Result of indexOf method: " + firstIndex);


console.log(" ");

/*
	lastIndexOf - returns the index number of the last matching element found in an array. The search process will be done from the last element proceeding to the first.

	Syntax:

		arrayName.lastIndexOf(searchValue);
		arrayName.lastIndexOf(searchValue, fromIndex);

*/


let lastIndex = countries.lastIndexOf("PH");
console.log("Result of lastIndexOf method: " + lastIndex);

lastIndex = countries.lastIndexOf("PH", 4);
console.log("Result of lastIndexOf method: " + lastIndex);



/*
	slice() - portions / slices elements from our array and returns a new array


	Syntax:
		arrayName.slice(startingIndex)
		arrayName.slice(startingIndex, endingIndex)
*/


console.log(countries);

let sliceArrA = countries.slice(2);
console.log("Result from slice method:");
console.log(sliceArrA);
console.log(countries);

console.log(" ");
let sliceArrB = countries.slice(2, 5);
console.log("Result from slice method:");
console.log(sliceArrB);
console.log(countries);

console.log(" ");
let sliceArrC = countries.slice(-3)
console.log("Result from slice method:");
console.log(sliceArrC);
console.log(countries);


/*
	toString() 	- returns an array as a string separated by commas.
				- is used internally bu JS wehn an array needs to be displayed as a text(Like inf HTML) or when an object/array needs to be used as a string

		Syntax:
			arrayName.toString()

*/


let stringArray = countries.toString();
console.log("Result from toString method:");
console.log(stringArray);


/*
	concat() - combines two or more arrays and returns the combined result

	Syntax:
		arrayA.concat(arrayB)
		arrayA.concat(elementA);
*/

let taskArrayA = ['drink HTML', 'eat Javascript'];
let taskArrayB = ['inhale CSS', 'breath SASS'];
let taskArrayC = ['get git', 'be node'];

console.log(" ");
let tasks = taskArrayA.concat(taskArrayB);
console.log("Result from concat method:");
console.log(tasks);


let allTasks = taskArrayA.concat(taskArrayB, taskArrayC);
console.log(allTasks);


//This is now combining arrays with element - similar with push()
let combineTask = taskArrayA.concat("smell express", "throw react");
console.log(combineTask);


/*
	join()	- returns an array as a string
			- does not change the original array
			- we can use any separator. The default is comma (,)

	Syntax:
		arrayName.join(separatorString)
*/


let students = ['Elysha', 'Gab', 'Ronel', 'Jean'];
console.log(students.join());
console.log(students.join(' '));
console.log(students.join(" - "));


/*
	Iteration Methods
		- loops designed to perform repetitive tasks in a array, used for manipulating array data resulting in complex tasks.
		- normally work with a function supplied as an argument
		- aims to evaluate each element in an array
*/


/*
	forEach()	- Similar to for loop that iterates on each array element.

	Syntax:

		arrayName.forEach(function(individualElemet){
			statement
		});_

*/
console.log(" ");

allTasks.forEach(function(task){
	console.log(task);
});


//Using forEach with conditional statements

console.log(" ");
let filteredTasks = [];

allTasks.forEach(function(task){
	console.log(task);

	if(task.length > 10){
		filteredTasks.push(task);
	}
});

console.log(allTasks);
console.log("Result of filteredTasks:")
console.log(filteredTasks)


/*
	map() - iterates on each element and returns a new array with different values depending on the result of the function's operation

	Syntax:
		arrayName.map(function(individualElement){
			statement
		});
*/
console.log(" ");


let numbers = [1, 2, 3, 4, 5];

let numberMap = numbers.map(function(number){
	console.log(number);
	return number*number;
});

console.log("Original Array");
console.log(numbers);
console.log("Result of the map method:");
console.log(numberMap);


/*
	every() - checks if all elements in an array met the given condition.
			- it returns a true value if all elements meet the condition and false if otherwise.

		Syntax:
			arrayName.every(function(individualElement){
				return expression/condition
			});
*/
console.log(" ");


let allValid = numbers.every(function(number){
	return (number < 3); 

});

console.log("Result of every method:");
console.log(allValid);



/*
	some() - checks if at least one lement in the array meet the given condition. Returns a true value if at least one of the elements meets the given condition and false if otherwise.

	Syntax:
			arrayName.some(function(individualElement){
				return expression/condition
			});
*/
console.log(" ");


let someValid = numbers.some(function(number){
	return (number < 3); 

});

console.log("Result of some method:");
console.log(someValid);


/*
	filter() - returns a new array that contains element which meets the given condition. Returns an empty array if no elements were found or no element satisfied the given condition.

	Syntax:
		arrayName.filter(function(individualElement){
				return expression/condition
			});
*/
console.log(" ");

let filteredValid = numbers.filter(function(number){
	return (number < 3);
});

console.log("Result of filter method:");
console.log(filteredValid);


/*
	includes() 	- checks if the argument passed can be found un an array
				- can be chained after another method. The result of the 1st method is used on the 2nd method until all chained methods have been resolved.



*/
console.log(" ");


let products = ['mouse', 'KEYBOARD', 'laptop', 'monitor'];

let filterdProducts = products.filter(function(product){
		return product.toLowerCase().includes("a")
});

console.log("Result of includes method:");
console.log(filterdProducts);