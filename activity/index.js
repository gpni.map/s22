/*
    Create functions which can manipulate our arrays.
    You may change the names on the initial list before you use the array varaibles.
*/

let registeredUsers = [

    "DJ Asutria",
    "Jayson Perry",
    "Jay Bacos",
    "Ryan Sebastian",
    "Michael Pastoral",
    "Joan Co",
    "Myrtle David"
];

let friendsList = [];

console.log("Registered Users: ");
console.log(registeredUsers);

/*
    
   1. Create a function which will allow us to register into the registeredUsers list.
        - this function should be able to receive a string.
        - determine if the input username already exists in our registeredUsers array.
            -if it is, show an alert window with the following message:
                "Registration failed. Username already exists!"
            -if it is not, add the new username into the registeredUsers array and show an alert:
                "Thank you for registering!"
        - Invoke and register a new user in the browser console.
        - In the browser console, log the registeredUsers array.

*/
    
//SOLUTION
    console.log(" ");
    

    function userRegistration(newUser){
        let doesUserExist = registeredUsers.includes(newUser);

        if (doesUserExist) {
            alert("Registration failed. User already exists!");
        } else {
            registeredUsers.push(newUser);
            alert("Thank you for registering!");
        }
    };

    userRegistration("Dennis Tayag");
    console.log("Updated registeredUsers:");
    console.log(registeredUsers);


/*
    2. Create a function which will allow us to add a registered user into our friends list.
        - This function should be able to receive a string.
        - Determine if the input username exists in our registeredUsers array.
            - If it is, add the foundUser in our friendList array.
                    -Then show an alert with the following message:
                        - "You have added <registeredUser> as a friend!"
            - If it is not, show an alert window with the following message:
                - "User not found."
        - Invoke the function and add a registered user in your friendsList in the browser console.
        - In the browser console, log the friendsList array in the console.

*/

//SOLUTION
    console.log(" ");

    function addUserToFriend(userIndex){
        let myFriend = registeredUsers.indexOf(userIndex)
        
            if(myFriend === -1){
                alert("User not found.");
            } else {
                friendsList.push(userIndex);
                alert("You have added "+ userIndex + " as a friend.")
            }
    }

    addUserToFriend("Dennis Tayag");
    addUserToFriend("Myrtle David");
    addUserToFriend("Jayson Perry");
    addUserToFriend("Ronald D.");
    addUserToFriend("Michael Pastoral");


    console.log("friendsList Array content:");
    console.log(friendsList);



/*
    3. Create a function which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty show an alert: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function.

*/

//SOLUTION
    console.log(" ");
    console.log("My Friends from the list:")

    function createFriendList(){
        if (friendsList.length === 0){
            console.log("You currently have 0 friends. Add one first.");
        }else {
            friendsList.forEach(function(friend){
                console.log(friend);
            });        
        }
    };

    createFriendList();


    
// STRETCH GOALS

/*
    4. Create a function which will display the amount of registered users in your friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty show an alert:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function in the browser console.
*/

//SOLUTION
    console.log(" ");

    function checkRegisteredFriend(){
        if (friendsList === 0){
            console.log("You currently have 0 friends. Add one first.");
        } else {
            let numberOfFriends = friendsList.length;
            console.log("You currently have " + numberOfFriends + " friends." );
        }
    }

    checkRegisteredFriend();


/*
    5. Create a function which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
        - In the browser console, log the friendsList array.

*/

//SOLUTION
    console.log(" ");


    function deleteLastAddedFriend(){
        if (friendsList === 0){
            console.log("You currently have 0 friends. Add one first.");
        } else {

            let deletedFriendIndex = friendsList[friendsList.length - 1];
            console.log("You have deleted " + deletedFriendIndex);
            friendsList.pop();
        }
    }

    deleteLastAddedFriend();
    console.log("Updated friendsList:");
    console.log(friendsList);




/*======================================================================================*/



// Try this for fun:
/*
   

    Instead of only deleting the last registered user in the friendsList delete a specific user instead.
        -You may get the user's index.
        -Then delete the specific user with splice().

*/

//SOLUTION
    console.log(" ");

    let finalList = friendsList.splice(0,1);
    console.log("Spliced User: " + finalList);

    console.log("Final friend/s list:");
    console.log(friendsList);




